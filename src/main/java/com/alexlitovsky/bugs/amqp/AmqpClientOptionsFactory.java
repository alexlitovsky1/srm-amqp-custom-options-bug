package com.alexlitovsky.bugs.amqp;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.eclipse.microprofile.config.Config;

import io.quarkus.runtime.Startup;
import io.smallrye.common.annotation.Identifier;
import io.smallrye.reactive.messaging.amqp.AmqpConnectorCommonConfiguration;
import io.vertx.amqp.AmqpClientOptions;

@ApplicationScoped
@Startup
public class AmqpClientOptionsFactory {

static final String CONFIG_PROPERTY_IDLE_TIMEOUT = "amqp-idle-timeout";
	
	@Inject
	Config mpConfig;
	
	private AmqpClientOptions options;
	
	@PostConstruct
	public void init() {
	
		AmqpConnectorCommonConfiguration config = new AmqpConnectorCommonConfiguration(mpConfig);
		
		String username = config.getUsername().get();
        String password = config.getPassword().get();
        String host = config.getHost();
        int port = config.getPort();
        boolean useSsl = config.getUseSsl();
        int reconnectAttempts = config.getReconnectAttempts();
        int reconnectInterval = config.getReconnectInterval();
        int connectTimeout = config.getConnectTimeout();
        int idleTimeout = mpConfig.getValue(CONFIG_PROPERTY_IDLE_TIMEOUT, Integer.class);
        
        AmqpClientOptions options = new AmqpClientOptions()
                .setUsername(username)
                .setPassword(password)
                .setHost(host)
                .setPort(port)
                .setSsl(useSsl)
                .setReconnectAttempts(reconnectAttempts)
                .setReconnectInterval(reconnectInterval)
                .setConnectTimeout(connectTimeout)
                .setIdleTimeout(idleTimeout)
                .setIdleTimeoutUnit(TimeUnit.MILLISECONDS);
        
        options.setWriteIdleTimeout(idleTimeout);

        this.options = options;
	}

	@Produces
//	@Named("amqp-options-with-idle-timeout")
	@Identifier("amqp-options-with-idle-timeout")
	public AmqpClientOptions getNamedOptions() {

		if (options == null) {
			throw new IllegalStateException("not initialized");
		}
		
        return options;
	}
}
