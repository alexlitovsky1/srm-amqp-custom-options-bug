package com.alexlitovsky.bugs.amqp;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;

@ApplicationScoped
@Startup
public class MessageSender {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);
	
	@Inject
    @Channel("test")
    Emitter<String> emitter;

	@PostConstruct
	public void sendMessage() {
		
		emitter.send("Hello world").exceptionally(t -> {
			LOGGER.error("error sending message", t);
			return null;
		});
	}
}
